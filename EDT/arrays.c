#include "edth.h"

void iniEmptyArray(int columns, char array[]){
    for(int i = 0; i < columns; i++){
        array[i] = ' ';
    }
}

void printArray(int columns, char array[]){
    for(int i = 0; i < columns; i++){
        printf("%c", array[i]);
    }
}

void putCharOnArray(int columns, char array[], int column, char c){
    array[column] = c;
}

void shiftRightArray(int columns, char array[], int column){
    for(int i = columns - 1; i > column; i--){
        array[i] = array[i-1];
    }
    //array[column] = ' ';
}

void shiftLeftArray(int columns, char array[], int column){
    for(int i = column; i < columns; i++){
        array[i] = array[i+1];
    }
    array[columns-1] = ' ';
}

void insertCharOnArray(int columns, char array[], int column, char c){
    shiftRightArray(columns, array, column);
    putCharOnArray(columns, array, column, c);
}

int searchCharOnArray(int columns, char array[], int column, char c){
    bool found = false;
    for(int i = column; i < columns || found == false; i++){
        if(array[i] == c){
            found = true;
            return i;
        }
    }
    return columns-1;
}

int searchNoCharOnArray(int columns, char array[], int column, char c){
    bool found = false;
    for(int i = column; i < columns || found == false; i++){
        if(array[i] != c){
            found = true;
            return i;
        }
    }
    return columns-1;
}
