#include "edth.h"

void iniEmptyMatrix(int files, int columns, char matrix[files][columns]){
    for(int i = 0; i < files; i++){
        iniEmptyArray(columns, matrix[i]);
    }
}

void printMatrix(int files, int columns, char matrix[files][columns]){
    for(int i = 0; i < files; i++){
        printf(" %1i|", i);
        printArray(columns, matrix[i]);
        printf("|\n");
    }
}

void insertCharOnMatrix(int files, int columns, char matrix[files][columns], int file, int column, char c){
    shiftRightArray(columns, matrix[file], column);
    putCharOnArray(columns, matrix[file], column, c);
}

void deleteCharOnMatrix(int files, int columns, char matrix[files][columns], int file, int column){
    shiftLeftArray(columns, matrix[file], column);
}

int searchCharOnMatrix(int files, int columns, char matrix[files][columns], int file, int column, char c){
    searchCharOnArray(columns, matrix[file], column, c);
}

int searchNoCharOnMatrix(int files, int columns, char matrix[files][columns], int file, int column, char c){
    searchNoCharOnArray(columns, matrix[file], column, c);
}

void shiftDownOnMatrix(int files, int columns, char matrix[files][columns], int file, int column){
    for(int i = files - 1; i > file; i--){
        for(int j = 0; j < columns; j++){
            matrix[i][j] = matrix[i-1][j];
        }
    }
}

void shiftUpOnMatrix(int files, int columns, char matrix[files][columns], int file, int column){
    for(int i = file; i < files; i++){
        for(int j = 0; j < columns; j++){
            matrix[i][j] = matrix[i+1][j];
        }
    }
    iniEmptyArray(columns, matrix[files-1]);
}
