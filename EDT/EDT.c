//*************************
//Xavier Vila Rubio, 47114599L
//*************************

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void iniEmptyArray(int columns, char array[])
{

    for(int i = 0; i < columns; i++)
    {
        array[i] = ' ';
    }

}

void printArray(int columns, char array[])
{

    for(int i = 0; i < columns; i++)
    {
        printf("%c", array[i]);
    }

}

void putCharOnArray(int columns, char array[], int column, char c)
{

    array[column] = c;

}

void shiftRightArray(int columns, char array[], int column)
{

    for(int i = columns - 1; i > column; i--)
    {
        array[i] = array[i-1];
    }
    array[column] = ' ';

}

void shiftLeftArray(int columns, char array[], int column)
{

    for(int i = column; i < columns; i++)
    {
        array[i] = array[i+1];
    }
    array[columns-1] = ' ';

}

void insertCharOnArray(int columns, char array[], int column, char c)
{

    shiftRightArray(columns, array, column);
    putCharOnArray(columns, array, column, c);

}

int searchCharOnArray(int columns, char array[], int column, char c)
{
    bool found = false;
    for(int i = column; i < columns || found == false; i++)
    {
        if(array[i] == c)
        {
            found = true;
            return i;
        }
    }
    return columns-1;

}

int searchNoCharOnArray(int columns, char array[], int column, char c)
{
    bool found = false;
    for(int i = column; i < columns || found == false; i++)
    {
        if(array[i] != c)
        {
            found = true;
            return i;
        }
    }
    return columns-1;

}

void iniEmptyMatrix(int files, int columns, char matrix[files][columns])
{

    for(int i = 0; i < files; i++)
    {
        iniEmptyArray(columns, matrix[i]);
    }

}

void printMatrix(int files, int columns, char matrix[files][columns])
{

    for(int i = 0; i < files; i++)
    {
        printf(" %1i|", i);
        printArray(columns, matrix[i]);
        printf("|\n");
    }

}

void insertCharOnMatrix(int files, int columns, char matrix[files][columns], int file, int column, char c)
{

    shiftRightArray(columns, matrix[file], column);
    putCharOnArray(columns, matrix[file], column, c);

}

void deleteCharOnMatrix(int files, int columns, char matrix[files][columns], int file, int column)
{

    shiftLeftArray(columns, matrix[file], column);

}

int searchCharOnMatrix(int files, int columns, char matrix[files][columns], int file, int column, char c)
{

    searchCharOnArray(columns, matrix[file], column, c);

}

int searchNoCharOnMatrix(int files, int columns, char matrix[files][columns], int file, int column, char c)
{

    searchNoCharOnArray(columns, matrix[file], column, c);

}

void shiftDownOnMatrix(int files, int columns, char matrix[files][columns], int file, int column)
{

    for(int i = files - 1; i > file; i--)
    {
        for(int j = 0; j < columns; j++)
        {
            matrix[i][j] = matrix[i-1][j];
        }
    }

}

void setActiveWindow(int appData[], int aw)
{

    appData[3] = aw;

}

void iniWindow(int appData[], char windows[][appData[1]][appData[2]], int window)
{

    iniEmptyMatrix(appData[1], appData[2], windows[window]);

}

void iniWindows(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2])
{

    for(int i = 0; i < appData[0]; i++)
    {

        iniWindow(appData, windows, i);

    }

}

void printWindowInfo(int appData[], int appCursors[][2])
{

    printf("%i-%i,%i\n", appCursors[appData[3]][0], appCursors[appData[3]][1], appData[3]);

}

void printWindow(int appData[], char matrix[appData[1]][appData[2]])
{

    printf("   ");
    for(int i = 0; i < appData[2]; i++)
    {
        printf("-");
    }
    printf("\n");
    printMatrix(appData[1], appData[2], matrix);
    printf("   ");
    for(int i = 0; i < appData[2]; i++)
    {
        printf("-");
    }
    printf("\n");

}

void printCurrentWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2])
{

    printWindowInfo(appData, appCursors);
    printWindow(appData, windows[appData[3]]);

}

bool isLegalFile(int appData[], int y)
{

    return (appData[1] >= y && y >= 0);

}

void setYCursorOnWindow(int appData[], int appCursors[][2], int y)
{

    if(isLegalFile(appData, y))
    {
        appCursors[appData[3]][0] = y;
    }

}

bool isLegalColumn(int appData[], int x)
{

    return (appData[2] >= x && x >= 0);

}

void setXCursorOnWindow(int appData[], int appCursors[][2], int x)
{

    if(isLegalColumn(appData, x))
    {
        appCursors[appData[3]][1] = x;
    }

}

void insertCharOnWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char c)
{

    insertCharOnMatrix(appData[1], appData[2], windows[appData[3]], appCursors[appData[3]][0], appCursors[appData[3]][1], c);

}

void deleteCurrentPositionOnWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2])
{

    deleteCharOnMatrix(appData[1], appData[2], windows[appData[3]], appCursors[appData[3]][0], appCursors[appData[3]][1]);

}

int searchCharOnWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char c)
{

    searchCharOnMatrix(appData[1], appData[2], windows[appData[3]], appCursors[appData[3]][0], appCursors[appData[3]][1], c);

}

void shiftDownOnWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2])
{
    shiftDownOnMatrix(appData[1], appData[2], windows[appData[3]], appCursors[appData[3]][0], appCursors[appData[3]][1]);
}


void gotoBeginLine(int appData[], int appCursors[][2])
{
    setXCursorOnWindow(appData, appCursors, 0);
}

void deleteCurrentFile(int appData[], int windows[][appData[1]][appData[2]], int appCursors[][2])
{
    iniEmptyArray(appData[2], windows[appData[3]][appCursors[appData[3]][0]]);
}

void gotoFirstFile(int appData[], int appCursors[][2])
{
    setYCursorOnWindow(appData, appCursors, 0);
}

void gotoLastFile(int appData[], int appCursors[][2])
{
    setYCursorOnWindow(appData, appCursors, appData[1] - 1);
}

void moveCursorRight(int appData[], int appCursors[][2])
{
    setXCursorOnWindow(appData, appCursors, appCursors[appData[3]][1] + 1);
}

void moveCursorDown(int appData[], int appCursors[][2])
{
    setYCursorOnWindow(appData, appCursors, appCursors[appData[3]][0] + 1);
}

void moveCursorUp(int appData[], int appCursors[][2])
{
    setYCursorOnWindow(appData, appCursors, appCursors[appData[3]][0] - 1);
}

void moveCursorLeft(int appData[], int appCursors[][2])
{
    setXCursorOnWindow(appData, appCursors, appCursors[appData[3]][1] - 1);
}
void gotoNextWord(int appData[], int windows[][appData[1]][appData[2]], int appCursors[][2])
{
    int num = searchCharOnWindow(appData, windows, appCursors, ' ');
    setXCursorOnWindow(appData, appCursors, num);
}


void parser(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char input)
{

    static int mode = 0; // Mode 0: normal - Mode 1: insert
    if(mode == 0){
        switch(input){
            case 'i':
                mode = 1;
                break;
            case 'j':
                moveCursorDown(appData, appCursors);
                break;
            case 'k':
                moveCursorUp(appData, appCursors);
                break;
            case 'h':
                moveCursorLeft(appData, appCursors);
                break;
            case 'l':
                moveCursorRight(appData, appCursors);
                break;
            case 'g':
                gotoFirstFile(appData, appCursors);
                break;
            case 'G':
                gotoLastFile(appData, appCursors);
                break;
            case 'w':
                gotoNextWord(appData, windows, appCursors);
                break;
            case 'x':
                deleteCurrentPositionOnWindow(appData, windows, appCursors);
                break;
            case 'o':
                shiftDownOnWindow(appData, windows, appCursors);
                iniEmptyArray(columns, matrix[file+1]); 
                moveCursorDown(appData, appCursors);
                gotoBeginLine(appData, appCursors);
                mode == 1;
                break;
            case 'O':
                shiftDownOnWindow(appData, windows, appCursors);
                iniEmptyArray(columns, matrix[file]); 
                moveCursorDown(appData, appCursors);
                gotoBeginLine(appData, appCursors);
                mode == 1;
                break;
        }

    }
    if(mode == 1){
        switch(input){
            case '&':
                mode = 0;
                break;
            default:
                insertCharOnWindow(appData, windows, appCursors, input);
        }

    }

}

int main()
{

    int appData[4];
    appData[0] = 3;
    appData[1] = 5;
    appData[2] = 15;
    char windows[appData[0]][appData[1]][appData[2]];
    int appCursors[appData[0]][2];

    char matrix[15][15];
    setActiveWindow(appData, 0);
    iniWindows(appData, windows, appCursors);
    setYCursorOnWindow(appData, appCursors, 2);
    setXCursorOnWindow(appData, appCursors, 3);
    insertCharOnWindow(appData, windows, appCursors, 'F');
    setXCursorOnWindow(appData, appCursors, 4);
    insertCharOnWindow(appData, windows, appCursors, 'U');
    setXCursorOnWindow(appData, appCursors, 5);
    insertCharOnWindow(appData, windows, appCursors, 'N');
    setXCursorOnWindow(appData, appCursors, 6);
    insertCharOnWindow(appData, windows, appCursors, 'C');
    setXCursorOnWindow(appData, appCursors, 7);
    insertCharOnWindow(appData, windows, appCursors, 'I');
    setXCursorOnWindow(appData, appCursors, 8);
    insertCharOnWindow(appData, windows, appCursors, 'O');
    setXCursorOnWindow(appData, appCursors, 9);
    insertCharOnWindow(appData, windows, appCursors, 'N');
    setXCursorOnWindow(appData, appCursors, 10);
    insertCharOnWindow(appData, windows, appCursors, 'A');
    setXCursorOnWindow(appData, appCursors, 11);
    insertCharOnWindow(appData, windows, appCursors, 'U');
    setYCursorOnWindow(appData, appCursors, 3);
    setXCursorOnWindow(appData, appCursors, 1);
    insertCharOnWindow(appData, windows, appCursors, 'P');
    setXCursorOnWindow(appData, appCursors, 2);
    insertCharOnWindow(appData, windows, appCursors, 'E');
    setXCursorOnWindow(appData, appCursors, 3);
    insertCharOnWindow(appData, windows, appCursors, 'R');
    setXCursorOnWindow(appData, appCursors, 4);
    insertCharOnWindow(appData, windows, appCursors, 'F');
    setXCursorOnWindow(appData, appCursors, 5);
    insertCharOnWindow(appData, windows, appCursors, 'E');
    setXCursorOnWindow(appData, appCursors, 6);
    insertCharOnWindow(appData, windows, appCursors, 'C');
    setXCursorOnWindow(appData, appCursors, 7);
    insertCharOnWindow(appData, windows, appCursors, 'T');
    setXCursorOnWindow(appData, appCursors, 8);
    insertCharOnWindow(appData, windows, appCursors, 'A');
    setXCursorOnWindow(appData, appCursors, 9);
    insertCharOnWindow(appData, windows, appCursors, 'M');
    setXCursorOnWindow(appData, appCursors, 10);
    insertCharOnWindow(appData, windows, appCursors, 'E');
    setXCursorOnWindow(appData, appCursors, 11);
    insertCharOnWindow(appData, windows, appCursors, 'N');
    setXCursorOnWindow(appData, appCursors, 12);
    insertCharOnWindow(appData, windows, appCursors, 'T');
    setXCursorOnWindow(appData, appCursors, 13);
    insertCharOnWindow(appData, windows, appCursors, 'E');
    printCurrentWindow(appData, windows, appCursors);
    setYCursorOnWindow(appData, appCursors, 2);
    setXCursorOnWindow(appData, appCursors, 11);
    parser(appData, windows, appCursors, 'x');
    printCurrentWindow(appData, windows, appCursors);
    parser(appData, windows, appCursors, 'o');
    searchCharOnWindow(appData, windows, appCursors, ' ');
    printCurrentWindow(appData, windows, appCursors);

}
