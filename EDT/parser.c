#include "edth.h"

void parser(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char input){
    static int mode = 0; // Mode 0: normal - Mode 1: insert
    if(mode == 0){
        switch(input){
            case 'i':
                mode = 1;
                break;
            case 'j':
                moveCursorDown(appData, appCursors);
                break;
            case 'k':
                moveCursorUp(appData, appCursors);
                break;
            case 'h':
                moveCursorLeft(appData, appCursors);
                break;
            case 'l':
                moveCursorRight(appData, appCursors);
                break;
            case 'g':
                gotoFirstFile(appData, appCursors);
                break;
            case 'G':
                gotoLastFile(appData, appCursors);
                break;
            case 'w':
                gotoNextWord(appData, windows, appCursors);
                break;
            case 'x':
                deleteCurrentPositionOnWindow(appData, windows, appCursors);
                break;
            case 'o':
                shiftDownOnWindow(appData, windows, appCursors);
                iniEmptyArray(appData[2], windows[appData[3]][appCursors[appData[3]][0]+1]); 
                moveCursorDown(appData, appCursors);
                gotoBeginLine(appData, appCursors);
                mode = 1;
                break;
            case 'O':
                shiftDownOnWindow(appData, windows, appCursors);
                iniEmptyArray(appData[2], windows[appData[3]][appCursors[appData[3]][0]]); 
                moveCursorDown(appData, appCursors);
                gotoBeginLine(appData, appCursors);
                mode = 1;
                break;
            case 'D':
                deleteCurrentFile(appData, windows, appCursors);
                break;
            case 'W':
                deleteWord(appData, windows, appCursors);
                break;
            case 't':
                if(appData[3] == 2){
                    setActiveWindow(appData, 0);
                }else{
                    setActiveWindow(appData, appData[3] + 1);
                }
                break;
            case 'T':
                if(appData[3] == 0){
                    setActiveWindow(appData, 2);
                }else{
                    setActiveWindow(appData, appData[3] - 1);
                }
                break;
            case '0':
                gotoBeginLine(appData, appCursors);
                break;
        }

    }else if(mode == 1){
        switch(input){
            case '&':
                mode = 0;
                break;
            default:
                insertCharOnWindow(appData, windows, appCursors, input);
                break;
        }
    }
}

void parseInput(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char input[]){
    for(int i = 0; i < strlen(input); i++){
        parser(appData, windows, appCursors, input[i]);
    }
}


void gotoBeginLine(int appData[], int appCursors[][2]){
    setXCursorOnWindow(appData, appCursors, 0);
}

void deleteCurrentFile(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]){
    shiftUpOnWindow(appData, windows, appCursors);
}

void gotoFirstFile(int appData[], int appCursors[][2]){
    setYCursorOnWindow(appData, appCursors, 0);
}

void gotoLastFile(int appData[], int appCursors[][2]){
    setYCursorOnWindow(appData, appCursors, appData[1] - 1);
}

void moveCursorRight(int appData[], int appCursors[][2]){
    setXCursorOnWindow(appData, appCursors, appCursors[appData[3]][1] + 1);
}

void moveCursorDown(int appData[], int appCursors[][2]){
    setYCursorOnWindow(appData, appCursors, appCursors[appData[3]][0] + 1);
}

void moveCursorUp(int appData[], int appCursors[][2]){
    setYCursorOnWindow(appData, appCursors, appCursors[appData[3]][0] - 1);
}

void moveCursorLeft(int appData[], int appCursors[][2]){
    setXCursorOnWindow(appData, appCursors, appCursors[appData[3]][1] - 1);
}

void gotoNextWord(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]){
    setXCursorOnWindow(appData, appCursors, searchCharOnWindow(appData, windows, appCursors, ' ') + 1);
}

void deleteWord(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]){
    //moveCursorRight(appData, appCursors);
    while(windows[appData[3]][appCursors[appData[3]][0]][appCursors[appData[3]][1]] != ' ' && appCursors[appData[3]][1] < appCursors[2]){
        shiftLeftArray(appData[2], windows[appData[3]][appCursors[appData[3]][0]], appCursors[appData[3]][1]);
    }
    shiftLeftArray(appData[2], windows[appData[3]][appCursors[appData[3]][0]], appCursors[appData[3]][1]);
}
