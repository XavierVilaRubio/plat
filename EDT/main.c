//*************************
//Xavier Vila Rubio, 47114599L
//*************************

#include "edth.h"

int main(int argc, char *argv[]){
    const int C_files = 5;
    const int C_columns = 15;
    const int C_windows = 3;
    int appData[4];
    appData[0] = C_windows;
    appData[1] = C_files;
    appData[2] = C_columns;
    appData[3] = 0;
    int appCursors[appData[0]][2];
    char windows[appData[0]][appData[1]][appData[2]];
    iniWindows(appData, windows, appCursors);

    for(int i = 0; i < appData[0]; i++)
    {
        for(int j = 0; j < 2; j++)
        {
            appCursors[i][j] = 0;
        }
    }

    parseInput(appData, windows, appCursors, argv[1]);
    printCurrentWindow(appData, windows, appCursors);
}
