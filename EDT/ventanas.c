#include "edth.h"

void setActiveWindow(int appData[], int aw){
    appData[3] = aw;
}

void iniWindow(int appData[], char windows[][appData[1]][appData[2]], int window){
    iniEmptyMatrix(appData[1], appData[2], windows[window]);
}

void iniWindows(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]){
    for(int i = 0; i < appData[0]; i++){
        iniWindow(appData, windows, i);
    }
}

void printWindowInfo(int appData[], int appCursors[][2]){
    printf("%i-%i,%i\n", appData[3], appCursors[appData[3]][0], appCursors[appData[3]][1]);
}

void printWindow(int appData[], char matrix[appData[1]][appData[2]]){
    printf("   ");
    for(int i = 0; i < appData[2]; i++){
        printf("-");
    }
    printf("\n");
    printMatrix(appData[1], appData[2], matrix);
    printf("   ");
    for(int i = 0; i < appData[2]; i++){
        printf("-");
    }
    printf("\n");
}

void printCurrentWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]){
    printWindowInfo(appData, appCursors);
    printWindow(appData, windows[appData[3]]);
}

bool isLegalFile(int appData[], int y){
    return (appData[1] > y && y >= 0);
}

void setYCursorOnWindow(int appData[], int appCursors[][2], int y){
    if(isLegalFile(appData, y)){
        appCursors[appData[3]][0] = y;
    }
}

bool isLegalColumn(int appData[], int x){
    return (appData[2] > x && x >= 0);
}

void setXCursorOnWindow(int appData[], int appCursors[][2], int x){
    if(isLegalColumn(appData, x)){
        appCursors[appData[3]][1] = x;
    }
}

void insertCharOnWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char c){
    insertCharOnMatrix(appData[1], appData[2], windows[appData[3]], appCursors[appData[3]][0], appCursors[appData[3]][1], c);
    setXCursorOnWindow(appData,appCursors,appCursors[appData[3]][1] + 1);
}

void deleteCurrentPositionOnWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]){
    deleteCharOnMatrix(appData[1], appData[2], windows[appData[3]], appCursors[appData[3]][0], appCursors[appData[3]][1]);
}

int searchCharOnWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char c){
    searchCharOnMatrix(appData[1], appData[2], windows[appData[3]], appCursors[appData[3]][0], appCursors[appData[3]][1], c);
}

void shiftDownOnWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]){
    shiftDownOnMatrix(appData[1], appData[2], windows[appData[3]], appCursors[appData[3]][0], appCursors[appData[3]][1]);
}

void shiftUpOnWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]){
    shiftUpOnMatrix(appData[1], appData[2], windows[appData[3]], appCursors[appData[3]][0], appCursors[appData[3]][1]);
}
