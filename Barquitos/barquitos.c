//*************************
//Xavier Vila, 47114599L
//*************************

#include<stdio.h>

#define MAXFILAS    10
#define MAXCOLUMNAS 10

void inicializar_tablero(int filas, int cols, char tablero[][MAXCOLUMNAS])
{

    for(int i = 0; i < MAXFILAS; i++)
    {
        for(int j = 0; j < MAXCOLUMNAS; j++)
        {
            tablero[i][j] = '0';
        }
    }

}

void colocar_barquito(int fila, int col, int longitud, char tipo, char tablero[][MAXCOLUMNAS])
{

    if(tipo == 'H')
    {
        for(int i = 0; i < longitud; i++)
        {
            tablero[fila][col + i] = '*';
        }
    }else if(tipo == 'V'){
        for(int i = 0; i < longitud; i++)
        {
            tablero[fila + i][col] = '*';
        }
    }

}

void imprimir_tablero(int filas, int cols, char tablero[][MAXCOLUMNAS])
{

    for(int i = 0; i < MAXFILAS; i++)
    {
        for(int j = 0; j < MAXCOLUMNAS; j++)
        {
            printf("%2c", tablero[i][j]);
        }
        printf("\n");
    }

}

int main()
{
    char tablero[MAXFILAS][MAXCOLUMNAS];

    inicializar_tablero(MAXFILAS, MAXCOLUMNAS, tablero);
    colocar_barquito(0, 0, 1, 'H', tablero);
    colocar_barquito(4, 3, 4, 'V', tablero);
    colocar_barquito(6, 5, 3, 'H', tablero);

    imprimir_tablero(MAXFILAS, MAXCOLUMNAS, tablero);
}
