//*************************
//Xavier Vila Rubio, 47114599L
//*************************

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void printLine(){
    printf("  ");
    for(int i = 0; i < 18; i++){printf("-");}
    printf("\n");
}

void iniGrid(char grid[8][8]){
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            grid[i][j]='.';
        }
    }
}

void printGrid(char grid[8][8]){
    printLine();
    for(int i = 7; i >= 0; i--){
        printf("%i", i+1);
        printf(" |");
        for(int j = 0; j < 8; j++){
            printf("%c ", grid[i][j]);
        }
        printf("|\n");
    }
    printLine();
    printf("   A B C D E F G H \n");
}

void vertical(char grid[8][8], int lletra, int num){
    for(int i = 0; i < 8; i++){
        grid[i][lletra] = '1';                    
    }
}

void horizontal(char grid[8][8], int lletra, int num){
    for(int j = 0; j < 8; j++){
        grid[num][j] = '1';                    
    }
}

void diagonalAscendentDreta(char grid[8][8], int lletra, int num){
    for(int i = num, j = lletra; i < 8 && j < 8; i++, j++){
            grid[i][j] = '1';
    }
}

void diagonalDescendentDreta(char grid[8][8], int lletra, int num){
    for(int i = num, j = lletra; i >= 0 && j < 8; i--, j++){
            grid[i][j] = '1';
    }
}

void diagonalAscendentEsquerra(char grid[8][8], int lletra, int num){
    for(int i = num, j = lletra; i < 8 && j >= 0; i++, j--){
            grid[i][j] = '1';
    }
}

void diagonalDescendentEsquerra(char grid[8][8], int lletra, int num){
    for(int i = num, j = lletra; i >= 0 && j >= 0; i--, j--){
            grid[i][j] = '1';
    }
}

int main(int argc, char *argv[]){

    char grid[8][8];
    int lletra = argv[1][0] - 'A';
    int num = argv[1][1] - '1';


    if(strlen(argv[1]) != 2 || num >= 8 || lletra >= 8){
        printf("Necesito una posición válida.\n");
        return 0;
    }

    iniGrid(grid);

    horizontal(grid, lletra, num);
    vertical(grid, lletra, num);
    diagonalAscendentDreta(grid, lletra, num);
    diagonalDescendentDreta(grid, lletra, num);
    diagonalAscendentEsquerra(grid, lletra, num);
    diagonalDescendentEsquerra(grid, lletra, num);
    grid[num][lletra] = '#';
    printGrid(grid);
}
